Source: litesql
Section: libs
Priority: optional
Build-Depends: @cdbs@
Build-Depends-Indep: doxygen-latex
Maintainer: Debian VoIP Team <pkg-voip-maintainers@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>
Vcs-Git: https://anonscm.debian.org/git/pkg-voip/litesql.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-voip/litesql.git
Homepage: https://lab.louiz.org/louiz/litesql

Package: litesql4
Section: libs
Architecture: any
Depends: ${shlibs:Depends},
 ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends},
 ${cdbs:Pre-Depends}
Multi-Arch: same
Conflicts: liblitesql0
Replaces: liblitesql0
Description: C++ object-relational persistence framework
 LiteSQL is a C++ library that integrates C++ objects tightly to
 relational database and thus provides an object persistence layer.
 LiteSQL supports SQLite3, PostgreSQL and MySQL as backends. LiteSQL
 creates tables, indexes and sequences to database and upgrades schema
 when needed.
 .
 In addition to object persistence, LiteSQL provides object relations
 which can be used to model any kind of C++ data structures. Objects can
 be selected, filtered and ordered using template- and class-based API
 with type checking at compile time.

Package: litesql-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: litesql4 (= ${binary:Version}),
 ${devlibs:Depends},
 ${misc:Depends}
Suggests: litesql-doc
Description: C++ object-relational persistence framework - development files
 LiteSQL is a C++ library that integrates C++ objects tightly to
 relational database and thus provides an object persistence layer.
 LiteSQL supports SQLite3, PostgreSQL and MySQL as backends. LiteSQL
 creates tables, indexes and sequences to database and upgrades schema
 when needed.
 .
 In addition to object persistence, LiteSQL provides object relations
 which can be used to model any kind of C++ data structures. Objects can
 be selected, filtered and ordered using template- and class-based API
 with type checking at compile time.
 .
 This package provides header files for developing your applications to
 use liblitesql template engine.

Package: litesql-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Description: C++ object-relational persistence framework - documentation
 LiteSQL is a C++ library that integrates C++ objects tightly to
 relational database and thus provides an object persistence layer.
 LiteSQL supports SQLite3, PostgreSQL and MySQL as backends. LiteSQL
 creates tables, indexes and sequences to database and upgrades schema
 when needed.
 .
 In addition to object persistence, LiteSQL provides object relations
 which can be used to model any kind of C++ data structures. Objects can
 be selected, filtered and ordered using template- and class-based API
 with type checking at compile time.
 .
 This package contains API documentation.

Package: litesql-tools
Section: utils
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
 ${misc:Depends}
Description: C++ object-relational persistence framework - utilities
 LiteSQL is a C++ library that integrates C++ objects tightly to
 relational database and thus provides an object persistence layer.
 LiteSQL supports SQLite3, PostgreSQL and MySQL as backends. LiteSQL
 creates tables, indexes and sequences to database and upgrades schema
 when needed.
 .
 In addition to object persistence, LiteSQL provides object relations
 which can be used to model any kind of C++ data structures. Objects can
 be selected, filtered and ordered using template- and class-based API
 with type checking at compile time.
 .
 This package provides helper tools for the liblitesql template engine.
